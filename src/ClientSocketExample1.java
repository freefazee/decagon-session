import java.io.*;
import java.net.Socket;

/**
 * GET /hello.htm HTTP/1.1
 * User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
 * Host: www.tutorialspoint.com
 * Accept-Language: en-us
 * Accept-Encoding: gzip, deflate
 * Connection: Keep-Alive
 */
public class ClientSocketExample1 {

    public static void main(String[] args) throws IOException {

        Socket client = new Socket("google.com", 80);

        InputStream in = client.getInputStream();
        OutputStream out = client.getOutputStream();

        PrintWriter writer = new PrintWriter(out);
        writer.println("GET / HTTP/1.1");
        writer.println("User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)");
        writer.println("Host: www.google.com");
        writer.println();
        writer.flush();


        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String[] parts = reader.readLine().split(" ");
        int statusCode = Integer.parseInt(parts[1]);

        if(statusCode == 200) {
            System.out.println("200 OK");
        } else  {
            System.out.println("Error " + statusCode);
        }


       String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }





    }
}
