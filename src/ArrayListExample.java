import java.util.ArrayList;

public class ArrayListExample {

    public static void main(String[] args) {

        ArrayList<String> people = new ArrayList<>();

        people.add("Joe");
        people.add("Mike");


        for(String p : people)
            System.out.println(p);
    }
}
