import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketExample {

    public static void main(String[] args) throws IOException {

        System.out.println("Listening on port 9005");

        ServerSocket serverSocket = new ServerSocket(9005);
        Socket connection = serverSocket.accept();
        serverSocket.close();


        while (true) {

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            PrintWriter out = new PrintWriter(connection.getOutputStream());

            String line;

            while ((line = in.readLine()) != null) {

                if(line.equalsIgnoreCase("close")) {
                    out.write("You have closed the connection.");
                    out.flush();
                    connection.close();
                    break;
                } else {
                    out.write("> " + line + "\n");
                    out.flush();
                }

            }


        }

    }
}
