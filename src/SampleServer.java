import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

class SocketClient extends Thread {

    private String name;
    Socket con;

    public SocketClient(String name, Socket con) {
        this.name = name;
        this.con = con;
    }

    private byte[] loadFile(File path, int length) throws IOException {

        FileInputStream in = null;

        byte[] data = new byte[length];

        try {
            in = new FileInputStream(path);
            in.read(data);
        } finally {
            if(in != null) in.close();
        }

        return data;
    }

    private byte[] readFileData(File file, int fileLength) throws IOException {
        FileInputStream fileIn = null;
        byte[] fileData = new byte[fileLength];

        try {
            fileIn = new FileInputStream(file);
            fileIn.read(fileData);
        } finally {
            if (fileIn != null)
                fileIn.close();
        }

        return fileData;
    }

    @Override
    public synchronized void run() {
        System.out.println("Thread started -> " + name);


        try {


            BufferedOutputStream dataOut = new BufferedOutputStream(con.getOutputStream());
            PrintWriter out = new PrintWriter(con.getOutputStream());

            String body = "<h1>This is the body</h1>";

            out.println("HTTP/1.1 200 OK");
            out.println("Server: Decagon v1.0");
            out.println("Content-length: " + body.length());
            out.println("Date: " + new Date());
            out.println("Content-type: text/html");
            out.println(); // very important
            out.flush();

            File file = new File("index.html");

            dataOut.write(readFileData(file, (int)file.length()));
            dataOut.flush();


        } catch (IOException e) {

            e.printStackTrace();
        }

    }

}

public class SampleServer {

    public static void main(String[] args) throws IOException {

       try(ServerSocket server = new ServerSocket(9005)) {

           System.out.println("Socket server is running on port 9005");

          while (true) {
              Socket con  = server.accept();

              SocketClient s = new SocketClient(new Date().toString(), con);
              s.start();


          }

       }

    }
}
