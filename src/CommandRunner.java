import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class CommandRunner {

    public static void main(String[] args){

        ProcessBuilder processBuilder = new ProcessBuilder();

        System.out.print("Enter command ? ");
        Scanner scanner = new Scanner(System.in);

        String command;

        while ((command = scanner.nextLine()) != null) {
            processBuilder.command("bash", "-c", command);

            try {
                Process process = processBuilder.start();

                BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));

                String line;

                while ((line = in.readLine()) != null) {
                    System.out.println(line);
                }

                process.waitFor();

                int code = process.exitValue();

                if(code == 0)  {
                    System.out.println("Success");
                } else {
                    System.out.println("Process exited with error " + code);
                }

            } catch (IOException | InterruptedException e) {
                e.getStackTrace();
            }
        }







    }
}
