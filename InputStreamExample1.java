import java.io.*;


public class InputStreamExample1 {

    public static void main(String[] args) throws IOException {



        //System.out.println("Welcome to the program");


        // Convert string to an InputStream
        String message = "This is a string\ntest and \nTsdfkds";

        InputStream in = new ByteArrayInputStream(message.getBytes());

        BufferedReader buf = new BufferedReader(new InputStreamReader(in));


        // Low-level API reading stream
       int c;

        while ( (c = in.read()) != -1) {

            System.out.print((char)c);
        }

      String line ;
        while((line= buf.readLine()) != null) {
            System.out.println(line);
        }
    }
}
