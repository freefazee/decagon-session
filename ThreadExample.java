import java.util.Date;

public class ThreadExample {

    public static void main(String[] args) throws InterruptedException {

        for(int i =0; i < 5; i++) {

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(new Date().toString());
                }
            });
        }

        Thread.sleep(1000);
    }
}
